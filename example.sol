//SPDX-License-Identifier: GPL


pragma solidity ^0.8.10;

contract Token {

	// Field



	string public name = "DA best shop";



	uint public profits = 0;



	function viewProfits() public view returns(uint) {
		return profits;
	}


	// function

	function Donations(uint userInput) public{
		profits += userInput;
	}

	function Refund(uint userInput) public{
		profits -= userInput;
	}

	function doubleDonate(uint userInput) public{
		uint product = userInput * 2;
		uint sum = profits += product;
	}

	function halfDonate(uint userInput) public{
		uint quotient = userInput / 2;
		uint sub = profits += quotient;
	}

	function Change(uint userInput) public{
		uint remainder = userInput%2;
		uint left = profits += remainder;
	}
	

}
